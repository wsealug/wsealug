# WSeaLUG Meeting notes July 31, 2021

## pre-meeting chat
- Sky Cries Mary: https://open.spotify.com/artist/6TAcOd7nZhLXchXNXdJsmi?autoplay=true

## Monitoring single VPS node for all the things without using a full DevOps solution? (Salt's)
- Bash script in cron, for instance rust 200 pings
- Dockerizsd Prometheus grafana alert manager but heavy
- Greylog but heavy due to elastic search
- https://theawesomegarage.com/blog/monitoring-your-docker-stack-with-grafana-and-prometheus
- https://wazuh.com/
- https://github.com/wazuh/wazuh
- https://www.freshworks.com/website-monitoring/
- https://zeek.org/
- Nagios - "Only The NSA Monitors More"

## WSeaLUG Picnic
- Date = 2021-09-11 
- what about having meeting 0900 - 1030 and then picnic after
    - decided no
- 1100hrs 
- brett will go over in the morning to hang reservation signs up. 
- Where is the sign?
    - Dave might have it. 
- EtherPad Link for food signup sheets
    - https://etherpad.seattlematrix.org/p/wsealug-2021-picnic-signup
- Dave will create a GetTogether event for RVSP
- Covid Policy?
    - Must Be Vaccinated
    - Masks encourged
    - (might change to required based on Delta news)  

## Music from 90s
- Dub from the 90s
    - Kruder & Dorfmiester (The K&D Session & DJ Kicks)
    - Theivery Corporation (DJ Kicks)
    - Chemical Brothers
    - Lamb
    - Morcheeba
    - Massive Attack
    - Moby
    - Mum (Yesterday Was Dramatic Today Is OK)
    - Northern Exposure (Trance compilation album, not the TV Show)
    - Hooverphonic (A New Stereophonic Sound Spectacular)
    - Run Lola Run (Soundtrack)
    - Boards of Canada (Music Has the Right to Children)
    - Goldie (Timeless)
    - Hybrid (Wide Angel)
    - Planet V (Drum & Base compilation)
    - Talvin Singh (OK)
    - Delerium (Karma & Symantic Spaces)
    - Sigur Ros (Ágætis Byrjun)
- Recent music I picked up off of Bandcamp: https://androcell.bandcamp.com/album/fib
