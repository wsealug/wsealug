# WSeaLUG Meeting Notes

Video call: https://meet.seattlematrix.org/wsealug  
Today’s discussion topics: https://wekan.seattlematrix.org/b/7m2fvvjftavo8qTpn/wsealug
Discussion topic ideas: https://etherpad.seattlematrix.org/p/WSeaLUG-Topics  

## Date 2022-03-26


### Misc.

  - Share your laptop/monitor setup!
  

### Favorite CLI Tools
  - Grep replacement: https://github.com/BurntSushi/ripgrep
  - Data munging with some cli vis: https://www.visidata.org/
  - xargs replacement with parallelization: https://www.gnu.org/software/parallel/
  - https://github.com/open-cli-tools/concurrently
  - improved cat: https://github.com/sharkdp/bat
  - improved grep: https://github.com/vrothberg/vgrep
  - like jq but for yaml: https://github.com/mikefarah/yq
  - like jq but for html: https://github.com/ericchiang/pup
  - pushd and popd
  - alterative to curl: https://httpie.io/docs/cli
  - nice diffs: https://github.com/dandavison/delta


### Dokku

  - https://dokku.com/

### Note Taking

  - https://getrocketbook.com/ 
  - plain text
  - OneNote/Evernote
  - https://joplinapp.org/
  - https://remarkable.com/
  - https://www.pine64.org/pinenote/


### SSH certs via remote CA

  - Cutter will learn more and report back

### AWS vs GCP

  - https://docs.aws.amazon.com/batch/latest/userguide/what-is-batch.html
  - https://docs.aws.amazon.com/cdk/v2/guide/home.html
