# WSeaLUG Bi-Weekly Meeting

## Date: 20200410
    https://github.com/spantaleev/matrix-docker-ansible-deploy/blob/master/docs/configuring-playbook-jitsi.md


## Keyboards:
    https://system76.com/laptops/pangolin
    https://andrew.kvalhe.im/2021-03-19
    https://www.coolermaster.com/catalog/peripheral/keyboards/masterkeys-pro-s/
    https://colemak.com/, https://github.com/MadRabbit/halmak

### Where to start
    https://mechanicalkeyboards.com/
    Split and/or low profile:
    Overview: https://aposymbiont.github.io/split-keyboards/
    Ideas: https://old.reddit.com/r/ErgoMechKeyboards/top/?sort=top&t=year
    Kits/parts: https://boardsource.xyz/, https://lowprokb.ca/, https://splitkb.com/, https://keeb.io/
    Community (help, group buys, modding e.g. silencing): https://discord.gg/vXc34gFdpc

### Key mapping
    https://github.com/baskerville/sxhkd
    https://github.com/sezanzeb/key-mapper

## What are you learning for fun
     learning rust
     https://linuxjourney.com/
     Go
     https://academy.tcm-sec.com/p/practical-ethical-hacking-the-complete-course


## Qube-OS
    https://www.qubes-os.org/

## Linux 101
    https://linuxjourney.com/
    acloud.guru
    Swtich 2 Linux - YouTube
    Spin up Linux VM along side of MacOS
    https://github.com/canonical/multipass


## Headsets/bluetooth
    Jabra Link 370 USB dongle - works with Mac/Linux, pairs with Jabra wireless headsets, possibly other bluetooth headset brands
    https://github.com/blueman-project/blueman
    https://www.sony.com/electronics/headband-headphones/wh-1000xm4
