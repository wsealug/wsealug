# West Seattle LUG
## bi-weekly meeting
## date: 20201205


## Links:

### Coffee/Tea
- https://www.seattlecoffeegear.com/coffee/shop-by-roaster#
- https://www.conduitcoffee.com/
- https://www.seattlecoffeegear.com/breville-barista-express-espresso-machine-bes870xl
- group order w/ Salt and Prasket? https://www.mayatea.com/collections/maya-chai/products/maya-chai-devi-concentrate

### Physix Project Linux Demo
- this was friggin' fantastic, bravo!
- https://physixproject.org/
- https://github.com/PhysixProject/physix

### Universal user profile management tips/tricks
- https://news.ycombinator.com/item?id=11070797
- nix

    - home manager ( https://github.com/nix-community/home-manager )

- https://nixos.org/learn.html


- use something to automate [symlinks](https://alexpearce.me/2016/02/managing-dotfiles-with-stow/)

    - http://brandon.invergo.net/news/2012-05-26-using-gnu-stow-to-manage-your-dotfiles.html

- a [bare git repository](https://www.atlassian.com/git/tutorials/dotfiles)
- management tool

    - https://github.com/ssh0/dot

    - https://github.com/ssh0/dotfiles

    - https://github.com/TheLocehiliosan/yadm

    - https://dev.to/biros/how-i-backupsync-my-dotfiles--apps-o0b

    - https://news.ycombinator.com/item?id=22008886

    - https://nixos.wiki/wiki/Home_Manager



### wearable computing
- https://web.archive.org/web/20191028074449/http://www.alexandraerin.com/2019/05/hack-this-look/
- https://store.vufine.com/products/vufine-wearable-display-1
- THE FUTURE: https://www.theverge.com/2018/6/6/17433516/ctrl-labs-brain-computer-interface-armband-hands-on-preview


### Internet Radio
- DI.fm
- SomaFM
- KEXP 
- Bandcamp
- KNKX
- https://downtempo.com/mp3s/ <-- sntxrr's DJ mixes
- https://flathub.org/apps/details/de.haeckerfelix.Shortwave


### Vim extensions 

- praskets .vimrc  from https://danielmiessler.com/study/vim/ 

    ```bash
    " Vim Configuration

    " My Basic Settings
    syntax enable                   " enable syntax highlighting
    syntax on                       " enable syntax hightling

    set encoding=utf-8              " set encoding
    set number                      " show line numbers
    set ts=4                        " set tabs to have 4 spaces
    set autoindent                  " indent when moving to next line while writing code
    set expandtab                   " expand tabs into spaces
    set shiftwidth=4                " when using the >> or << commands, shift lines by 4 spaces
    set cursorline                  " show a visual line under the cursor's current line
    set showmatch                   " show the matching part of the pair for [] {} and ()
    set paste                       " set paste mode at default

    let python_highlight_all = 1    " enable all Python syntax highlighting features
    let mapleader = "\<Space>"      " change default leader key

    inoremap jk <ESC>
    ```

- vimtutor 
- coc.nvim:  https://github.com/neoclide/coc.nvim.git
- vim-airline:  https://github.com/vim-airline/vim-airline.git
- vim-airline-themes:  https://github.com/vim-airline/vim-airline-themes.git
- vim-commentary:  https://github.com/tpope/vim-commentary.git
- vim-ps1:  https://github.com/PProvost/vim-ps1
- vim-puppet:   https://github.com/rodjek/vim-puppet
- vim-scriptease:   https://github.com/tpope/vim-scriptease
- vim-sensible:   https://github.com/tpope/vim-sensible.gitale:https://github.com/dense-analysis/ale.git              
- https://vim-adventures.com/

spacemacs....

- https://en.wikipedia.org/wiki/Pine_%28email_client%29
- https://en.wikipedia.org/wiki/Pico_(text_editor)


