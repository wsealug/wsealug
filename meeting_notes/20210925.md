# WSeaLUG Meeting Notes

Video call: https://meet.seattlematrix.org/wsealug  
Today’s discussion topics: https://wekan.seattlematrix.org/b/7m2fvvjftavo8qTpn/wsealug
Discussion topic ideas: https://etherpad.seattlematrix.org/p/WSeaLUG-Topics  

## Date 2021-09-25

## Pre-Meeting
 - whiteboards

     - https://www.quartet.com/p/dry-erase-boards/glass-boards/infinity-cubicle-magnetic-glass-whiteboards-30-x-18-white-pdec1830/


- https://darntough.com/ 


- https://heathercoxrichardson.substack.com/archive?sort=new

- https://mailu.io/1.8/

## DSLR as webcam 
- https://www.elgato.com/en/cam-link-4k
- https://github.com/stoth68000/sc0710
- https://magiclantern.fm/ & https://chdk.fandom.com/wiki/CHDK
https://www.blackmagicdesign.com/products/atemmini

- https://github.com/kmonad/kmonad

