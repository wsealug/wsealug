
# WSeaLUG Meeting Notes

  * Video call: https://meet.seattlematrix.org/wsealug  
  * Today’s discussion topics: https://wekan.seattlematrix.org/b/7m2fvvjftavo8qTpn/wsealug
  * Discussion topic ideas: https://etherpad.seattlematrix.org/p/WSeaLUG-Topics  

## Date 2022-06-04

## Lean Coffee Topics

### Favorite local eatery

  * Pam's Kitchen (https://pams-kitchen.com/)
  * Ciudad Grill - Geogetown (http://www.ciudadseattle.com/menu)
  * Centro - Burien (https://www.centroburien.com/)
  * Eden Hill - Queen Anne (http://www.edenhillrestaurant.com/)
  * Biscuit Bitch (http://caffelieto.com/)
  * Watson's Counter (https://www.watsonscounter.com/)

### Should we fly drones as a LUG outing? 

Do it on Saturday we would normally have meeting weather pending.

  * https://www.dji.com/air-2s
  * https://hugin.sourceforge.io/ semi-manual panorama stitcher
  * https://en.wikipedia.org/wiki/Meadowbrook,_Seattle
  * https://uavcoach.com/where-to-fly-drone/seattle/

### Retro Gaming
  * Gnome Games (https://wiki.gnome.org/Apps/Games)
  * RetroPI (https://retropie.org.uk/)
  * Controller Docs (https://retropie.org.uk/docs/Logitech-Controller/)
    - Check the left panel for full list of controllers
    - https://www.logitechg.com/en-us/products/gamepads/f310-gamepad.html

### Legitimate Digital Music Downloads

  * Bandcamp (https://bandcamp.com)
    - Daily (https://daily.bandcamp.com/): human curated lists, best-of-month, artist bios.
  * Subscriptions: Amazon Prime, Apple Music
  * Radio: SomaFM, DI.FM, SHOUTcast
    - https://www.di.fm/
    - https://somafm.com/ 
  * https://www.google-dorking.com/

### Photogrametry
  * Docs:
    - https://en.wikipedia.org/wiki/Photogrammetry
    - https://en.wikipedia.org/wiki/Structure_from_motion
    - More technical: https://cmsc426.github.io/sfm/
  * Apps:
    - Photosynth
  * Stories
    - [Bevington Object](https://www.tighar.org/Projects/Earhart/Archives/Research/Bulletins/82_BevingtonAnalysis2/82_BevingtonObjectNewAnalysis.html): Intesting photo analysis for possible image of Amelia Erhart's crashed plane strut.
